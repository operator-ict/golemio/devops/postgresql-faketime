ARG ORIGIN_IMAGE_VERSION=latest

FROM bitnami/postgresql:$ORIGIN_IMAGE_VERSION as builder

USER root

RUN apt update && \
    apt install -y \
        git \
        make \
        build-essential && \
    rm -r /var/lib/apt/lists /var/cache/apt/archives

RUN git clone https://github.com/wolfcw/libfaketime.git && \
    cd libfaketime/src && \
    make install


FROM bitnami/postgresql:$ORIGIN_IMAGE_VERSION

USER root

COPY --from=builder /libfaketime/src/libfaketime.so.1 /usr/local/lib
ENV LD_PRELOAD=/usr/local/lib/faketime/libfaketime.so.1
COPY start.sh .

USER 1001

CMD ["./start.sh"]
